
# Motorcycle racing voter

## Deployment for development

Create virtualenv ans install dependencies:

    pip install -r dependencies.txt

Copy `settings_local.py.template` to `settings_local.py` and edit the settings
for database.

Initialize the database:

    python manage.py syncdb
    python manage.py migrate
    python manage.py loaddata polls_data.yaml

You are now good to go:

    python manage.py runserver
