from django.contrib import admin
from moto.stats.models import Championship, Stage, Track, Country


class CountryAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'enabled')
    list_display_links = ('name', )
    list_editable = ('enabled', )
    list_filter = ('enabled', )

    search_fields = ('name', )

    fields = ('name', 'enabled')


class TrackAdmin(admin.ModelAdmin):
    list_display = ('name', 'country', 'slug', )
    list_display_links = ('name', )
    fields = (
        ('name', 'slug',),
        'country',
    )
    prepopulated_fields = {'slug': ('name',)}


class StageInline(admin.TabularInline):
    model = Stage
    extra = 3


class ChampionshipAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'display_order', )
    list_display_links = ('name', )
    list_editable = ('display_order', )

    fields = (
        ('slug', 'name', 'display_order'),
    )

    inlines = (StageInline, )


admin.site.register(Country, CountryAdmin)
admin.site.register(Track, TrackAdmin)
admin.site.register(Championship, ChampionshipAdmin)
