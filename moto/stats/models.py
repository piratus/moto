from django.db import models
from django.dispatch import receiver


class Country(models.Model):
    code = models.CharField(max_length=10, primary_key=True)
    name = models.CharField(max_length=255)

    enabled = models.BooleanField(default=False, db_index=True)

    class Meta:
        verbose_name_plural = 'countries'
        ordering = ('-enabled', 'code', )

    @property
    def flag_class(self):
        return 'flag-{}'.format(self.code.lower())

    @property
    def display_name(self):
        if ',' not in self.name:
            return self.name
        start, end = self.name.split(',')
        return '{} {}'.format(end.strip(), start.strip())

    def __unicode__(self):
        return self.name


class Track(models.Model):
    slug = models.CharField(max_length=50, primary_key=True)
    name = models.CharField(max_length=255)

    country = models.ForeignKey(Country, related_name='tracks',
                                limit_choices_to={'enabled': True})

    def __unicode__(self):
        return self.slug


class Championship(models.Model):
    slug = models.CharField(max_length=20, primary_key=True)
    name = models.CharField(max_length=255)

    display_order = models.IntegerField(default=0, db_index=True)

    class Meta:
        ordering = ('display_order', )

    def __unicode__(self):
        return self.slug


class Stage(models.Model):
    champ = models.ForeignKey(Championship, related_name='stages')
    track = models.ForeignKey(Track, related_name='stages')

    race_date = models.DateField(db_index=True)

    class Meta:
        ordering = ('race_date', )

    def __unicode__(self):
        return '{},{},{}'.format(self.champ_id, self.track_id, self.race_date)


SESSION_TYPES = [
    ('RACE', 'RACE',),
    ('QP', 'QP',),
]


class Session(models.Model):
    stage = models.ForeignKey(Stage)
    session_type = models.CharField(max_length=20, db_index=True,
                                    choices=SESSION_TYPES)

    def __unicode__(self):
        return self.session_type + 'session'

@receiver(models.signals.post_save, sender=Track)
def enable_country(sender, instance=None, **kwargs):
    assert instance is not None, "No instance provided for post_save signal"
    if not hasattr(instance, 'country'):
        country = Country.objects.get(code=instance.country_id.upper())
    else:
        country = instance.country

    if not country.enabled:
        country.enabled = True
        country.save()
