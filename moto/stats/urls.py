from django.conf.urls import url, patterns
from moto.stats.views import calendar

urlpatterns = patterns('',
   url('^$', calendar, name='calendar'),
)
