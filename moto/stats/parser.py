from contextlib import closing
from json.decoder import JSONDecoder
from urllib2 import urlopen
from BeautifulSoup import BeautifulSoup


STAGES_URL = 'http://www.motogp.com/en/ajax/results/selector/%s'
BASE_URL = 'http://www.motogp.com'


class ParseError(Exception):
    pass


def get_stages_for_season(season):
    with closing(urlopen(STAGES_URL % season)) as url:
        data = JSONDecoder().decode(url.read())

    if not data:
        return []

    lines = sorted(data.iteritems(), key=lambda x: int(x[0]))
    return [line[1] for line in lines]


def _get_stage_info(stage):
    with closing(urlopen(BASE_URL + stage['url'])) as url:
        data = JSONDecoder().decode(url.read())

    stage['categories'] = {c['name']: {} for c in data}
    for cat in stage['categories']:
        cat_url = BASE_URL + stage['url'] + '/' + cat
        with closing(urlopen(cat_url)) as url:
            data = JSONDecoder().decode(url.read())
            if not data:
                continue
        stage['categories'][cat] = {
            s['value']: s['url'].replace('selector', 'parse') for s in data}

    return stage


def get_session_results(stage, category='MotoGP', session='RAC'):
    stage = _get_stage_info(stage)
    try:
        url = BASE_URL + stage['categories'][category][session]
    except KeyError:
        raise ParseError('No results')

    soup = BeautifulSoup(urlopen(url).read())
    try:
        items = datagetter(soup.find('table').findAll('td'))
    except AttributeError:
        raise ParseError('No results')

    return [{
        'position': line[0],
        'points': line[1],
        'number': line[2],
        'name': line[3],
        'country': line[4],
        'team': line[5],
        'bike': line[6],
        'speed': line[7],
        'time': line[8]
    } for line in items]


FINISHED = 0
NOT_CLASSIFIED = -1
NOT_STARTING = -2
EXCLUDED = -3


def datagetter(iterable):
    """
    Yields a list of 9 elements at a time. The elements are cleaned from html
    and first element of each line is:
        -1 for not classified riders
        -2 for not starting riders

    If rider's number is skipped it is replaced with -1.
    """
    line = []
    state = FINISHED
    for item in iterable:
        text = item.text
        if text.lower() == 'not classified':
            state = NOT_CLASSIFIED
            continue
        if text.lower() == 'not starting':
            state = NOT_STARTING
            continue
        line.append(text)
        if len(line) == 9:
            # TODO: Fix this stupid part.
            if line[0].lower() == 'excluded':
                line[0] = EXCLUDED
            elif line[0].lower() in {'not finished 1st lap', '0 lap'}:
                line[0] = NOT_CLASSIFIED
            elif state != FINISHED:
                line[0] = state
            else:
                line[0] = int(line[0] or -1)
            line[1] = int(line[1] or 0)
            line[2] = int(line[2] or -1)
            yield line
            line = []


def print_session(results):
    if not results:
        print 'No data'
        return

    for result in results:
        print '%(position)3s %(name)20s %(time)10s' % result
