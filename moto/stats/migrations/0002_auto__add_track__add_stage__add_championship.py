# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Track'
        db.create_table(u'stats_track', (
            ('slug', self.gf('django.db.models.fields.CharField')(max_length=50, primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(related_name='tracks', to=orm['stats.Country'])),
        ))
        db.send_create_signal(u'stats', ['Track'])

        # Adding model 'Stage'
        db.create_table(u'stats_stage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('champ', self.gf('django.db.models.fields.related.ForeignKey')(related_name='stages', to=orm['stats.Championship'])),
            ('track', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stats.Track'])),
            ('race_date', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal(u'stats', ['Stage'])

        # Adding model 'Championship'
        db.create_table(u'stats_championship', (
            ('slug', self.gf('django.db.models.fields.CharField')(max_length=20, primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('display_order', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'stats', ['Championship'])


    def backwards(self, orm):
        # Deleting model 'Track'
        db.delete_table(u'stats_track')

        # Deleting model 'Stage'
        db.delete_table(u'stats_stage')

        # Deleting model 'Championship'
        db.delete_table(u'stats_championship')


    models = {
        u'stats.championship': {
            'Meta': {'ordering': "('display_order',)", 'object_name': 'Championship'},
            'display_order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '20', 'primary_key': 'True'})
        },
        u'stats.country': {
            'Meta': {'ordering': "('code',)", 'object_name': 'Country'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '10', 'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'stats.stage': {
            'Meta': {'ordering': "('race_date',)", 'object_name': 'Stage'},
            'champ': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'stages'", 'to': u"orm['stats.Championship']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'race_date': ('django.db.models.fields.DateField', [], {}),
            'track': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['stats.Track']"})
        },
        u'stats.track': {
            'Meta': {'object_name': 'Track'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'tracks'", 'to': u"orm['stats.Country']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '50', 'primary_key': 'True'})
        }
    }

    complete_apps = ['stats']
