# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding index on 'Championship', fields ['display_order']
        db.create_index(u'stats_championship', ['display_order'])

        # Adding field 'Country.enabled'
        db.add_column(u'stats_country', 'enabled',
                      self.gf('django.db.models.fields.BooleanField')(default=False, db_index=True),
                      keep_default=False)

        # Adding index on 'Stage', fields ['race_date']
        db.create_index(u'stats_stage', ['race_date'])


    def backwards(self, orm):
        # Removing index on 'Stage', fields ['race_date']
        db.delete_index(u'stats_stage', ['race_date'])

        # Removing index on 'Championship', fields ['display_order']
        db.delete_index(u'stats_championship', ['display_order'])

        # Deleting field 'Country.enabled'
        db.delete_column(u'stats_country', 'enabled')


    models = {
        u'stats.championship': {
            'Meta': {'ordering': "('display_order',)", 'object_name': 'Championship'},
            'display_order': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '20', 'primary_key': 'True'})
        },
        u'stats.country': {
            'Meta': {'ordering': "('-enabled', 'code')", 'object_name': 'Country'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '10', 'primary_key': 'True'}),
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'stats.stage': {
            'Meta': {'ordering': "('race_date',)", 'object_name': 'Stage'},
            'champ': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'stages'", 'to': u"orm['stats.Championship']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'race_date': ('django.db.models.fields.DateField', [], {'db_index': 'True'}),
            'track': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['stats.Track']"})
        },
        u'stats.track': {
            'Meta': {'object_name': 'Track'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'tracks'", 'to': u"orm['stats.Country']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '50', 'primary_key': 'True'})
        }
    }

    complete_apps = ['stats']
