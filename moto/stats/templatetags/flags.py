from django import template

register = template.Library()


@register.simple_tag
def flag(country):
    return u'<i class="{}" title="{}"></i>'.format(country.flag_class,
                                                   country.display_name)
