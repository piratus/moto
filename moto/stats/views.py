# coding: utf-8
from itertools import groupby
from django.shortcuts import render
from moto.stats.models import Stage


MONTHS = [
    u'Январь', u'Февраль', u'Март', u'Апрель',
    u'Май', u'Июнь', u'Июль', u'Август',
    u'Сентябрь', u'Октябрь', u'Ноябрь', u'Декабрь',
]


def calendar(request):
    stages = Stage.objects.all().select_related()
    by_month = [(MONTHS[month - 1], list(stages)) for month, stages
                in groupby(stages, lambda x: x.race_date.month)]
    return render(request, 'stats/calendar.html',
                  {'stages': stages, 'by_month': by_month})
