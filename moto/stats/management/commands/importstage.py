from django.core.management import BaseCommand, CommandError
from moto.stats.models import Championship
from moto.stats.parser import get_stages_for_season, get_session_results


CHAMPIONSHIPS = {s.slug: s for s in Championship.objects.all()}
MOTOGP_STAGES = {
    'losail': 'QAT',
    'cota': 'AME',
    'jerez': 'SPA',
}


class Command(BaseCommand):
    """Import stage from champ's website"""
    help = __doc__
    args = "<champ> <year> <stage> <session>"

    def handle(self, champ=None, year=None,
               stage=None, session=None, **options):
        try:
            champ_obj = CHAMPIONSHIPS[champ]
        except KeyError:
            print 'supported champs are "[]"'.format(', '.join(CHAMPIONSHIPS))
            return

        if year != '2013':
            print 'Available years are [2013]'
            return

        stages = [(i, s.track_id, s)
                  for i, s in enumerate(champ_obj.stages.all())]
        if stage is None:
            _show_stages(stages)
            return

        stage_obj = None
        try:
            stage = int(stage)
            stage_obj = stages[stage][2]
        except (TypeError, ValueError):
            try:
                by_track = {s[2].track_id: s[2] for s in stages}
                stage_obj = by_track[stage]
            except KeyError:
                pass

        if stage_obj is None:
            print 'unknown stage {}'.format(stage)
            _show_stages(stages)
            return

        handler = getattr(self, 'handle_{}'.format(champ))
        handler(stage_obj, session)

    def handle_motogp(self, stage, session):
        sessions = {'qp', 'rac'}
        if session not in sessions:
            print 'Available session codes are {{{}}}'.format(
                ', '.join(s for s in sessions))
            return
        stages = {stage['shortname']: stage
                  for stage in get_stages_for_season(2013)}  # TODO: add years
        print get_session_results(stages[MOTOGP_STAGES[stage.track_id]])

    def handle_wsbk(self, stage, session):
        sessions = {'qp', 'race1', 'race2'}
        if session not in sessions:
            print 'Available session codes are {{{}}}'.format(
                ', '.join(s for s in sessions))
            return


def _show_stages(stages):
    print '\n'.join('{} - {}'.format(s[0], s[1]) for s in stages)
