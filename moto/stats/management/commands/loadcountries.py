"""Management command for loading countries list"""
from contextlib import closing
from string import capitalize
from urllib2 import urlopen
from django.core.management.base import NoArgsCommand
from moto.stats.models import Country


LIST_URL = ('http://www.iso.org/iso/home/standards/country_codes/'
            'country_names_and_code_elements_txt.htm')

SHORT_WORDS = {'AND', 'OF', 'DA'}
CAP_WORDS = {'U.S.'}


def capwords(string):
    """Capitalize all words in a string, skipping short ones"""
    def upper(word):
        if word in SHORT_WORDS:
            return word.lower()
        elif word in CAP_WORDS:
            return word.upper()
        return capitalize(word)

    return ' '.join(map(upper, string.split(' ')))


class Command(NoArgsCommand):
    """Load countries names and ISO codes"""
    help = __doc__

    def handle_noargs(self, **options):
        with closing(urlopen(LIST_URL)) as url:
            data = url.readlines()

        for line in data[1:]:
            if ';' not in line:
                continue
            name, code = line.strip().split(';')
            self.add_country(code, name)

    def add_country(self, code, name):
        country, created = Country.objects.get_or_create(code=code)
        country.name = capwords(name)
        country.save()

        print 'Added {}'.format(country)
