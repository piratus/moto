from collections import namedtuple
from datetime import datetime
from django.core.management import BaseCommand, CommandError
from moto.stats.models import Championship, Track, Stage


Line = namedtuple('Line', ['champ', 'track', ])


class Command(BaseCommand):
    """Load stages calendar from .csv file"""
    help = __doc__
    args = '<filename>'

    champs_cache = {}

    def handle(self, filename=None, **options):
        if not filename:
            raise CommandError('No filename specified')

        with open(filename) as data:
            lines = data.readlines()

        map(self.create_stage, lines)

    def create_stage(self, line):
        """Create `Stage` or update it's `race_date`"""
        champ, track, date_str = line.rstrip('\n').split(',')

        race_date = datetime.strptime(date_str, '%d.%m.%y')

        stage, created = Stage.objects.get_or_create(
            champ=self.get_champ(champ),
            track=self.get_track(track),
            defaults={
                'race_date': race_date
            }
        )
        if created:
            print 'Created stage {}'.format(stage)
        else:
            stage.race_date = race_date
            stage.save()
            print 'Updated stage {}'.format(stage)

    def get_champ(self, slug):
        """Get a `Championship` object"""
        slug = slug.lower()
        if not slug in self.champs_cache:
            self.champs_cache[slug] = Championship.objects.get(slug=slug)

        return self.champs_cache[slug]

    def get_track(self, slug):
        """Get `Track` or try to create one"""
        country_code = None
        if '(' in slug:
            slug, country_code = slug[:-1].split('(')

        try:
            return Track.objects.get(slug=slug)
        except Track.DoesNotExist:
            if not country_code:
                raise

            return Track.objects.create(slug=slug, name=slug,
                                        country_id=country_code.upper())
