from django.http import HttpResponseRedirect


def get_username(request, *args, **kwargs):
    """Extract username from user object or session"""
    if kwargs.get('user'):
        username = kwargs['user'].username
    else:
        username = request.session.get('saved_username')
    return {'username': username}


def redirect_to_form(*args, **kwargs):
    """Show register form to get a username"""
    if (not kwargs['request'].session.get('saved_username') and
            kwargs.get('user') is None):
        return HttpResponseRedirect('/account/form/')
