from django.conf.urls import patterns, url

from .views import home, list_users, profile, logout, error, form

urlpatterns = patterns('',
    url(r'^account/$', home, name='home'),
    url(r'^account/error/$', error, name='error'),
    url(r'^account/logout/$', logout, name='logout'),
    url(r'^account/form/$', form, name='form'),
    url(r'^users/$', list_users, name='users'),
    url(r'^users/(?P<user_id>\d+)/$', profile, name='profile'),
)
