# coding: utf-8
from django import forms
from django.contrib.auth.models import User


class RegisterForm(forms.Form):
    """User registration form"""
    username = forms.CharField(max_length=100)

    def clean_username(self):
        """Check username for uniqueness"""
        username = self.cleaned_data['username']
        if User.objects.filter(username=username):
            raise forms.ValidationError(u'имя уже занято')

        return username
