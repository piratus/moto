from django.http import Http404
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.models import User
from django.shortcuts import redirect, render
from django.contrib.messages.api import get_messages

from social_auth.utils import setting

from moto.polls.models import VoteResult
from .forms import RegisterForm


def home(request):
    """Home view, displays login mechanism"""
    if request.user.is_authenticated():
        return redirect('profile', request.user.id)
    else:
        return render(request, 'home.html')


def list_users(request):
    """All users stats"""
    users = User.objects.order_by('pk')
    return render(request, 'users.html', {'users': users})


def profile(request, user_id):
    """User profile page"""
    try:
        user = User.objects.get(pk=user_id)
    except User.DoesNotExist:
        raise Http404

    results = VoteResult.objects.select_related().filter(user=user)
    return render(request, 'profile.html', {
        'current_user': user,
        'results': results,
        'own_page': request.user == user
    })


def error(request):
    """Login error view"""
    messages = get_messages(request)
    return render(request, 'error.html', {'messages': messages})


def logout(request):
    """Log out user"""
    auth_logout(request)
    return redirect('/')


def form(request):
    """Display and process user registration form"""
    if request.user.is_authenticated():
        return redirect('profile')

    if request.method == 'POST' and 'username' in request.POST:
        form = RegisterForm(request.POST)
        if form.is_valid():
            name = setting('SOCIAL_AUTH_PARTIAL_PIPELINE_KEY',
                           'partial_pipeline')
            request.session['saved_username'] = form.cleaned_data['username']
            backend = request.session[name]['backend']

            return redirect('socialauth_complete', backend=backend)
    else:
        form = RegisterForm()

    return render(request, 'form.html', {'form': form})
