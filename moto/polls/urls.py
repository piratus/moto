from django.conf.urls import patterns, url
from moto.polls.views import list_stages, view_poll, save_poll

urlpatterns = patterns('',
    url(r'^$', list_stages),
    url(r'^(?P<stage_name>\w{3})/$', view_poll, name='view-poll'),
    url(r'^(?P<stage_name>\w{3})/save/$', save_poll, name='save-poll'),
)
