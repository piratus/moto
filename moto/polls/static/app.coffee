class window.Countdown
    constructor: (@endTime, @callback)->
        @endTime = new Date(@endTime)
        @tick()

    tick: =>
        delta = ((@endTime - Date.now()) / 1000).toFixed 0

        secs = delta % 60
        mins = Math.floor(delta / 60) % 60
        hours = Math.floor(delta / 60 / 60) % 24
        days = Math.floor(delta / 60 / 60 / 24)

        @callback(days, hours, mins, secs)

        @timerId = setTimeout(@tick, 1000) if not @stopRequested

    stop: ->
        clearTimeout(@timerId) if @timerId

class App
    constructor: ->
        $('#riders .rider').draggable
            revert: 'invalid'
            helper: 'clone'

        $('#vote .rider').droppable
            accept: '.rider',
            helper: 'clone',
            hoverClass: 'hover'
            drop: @addRider

        $('#vote .rider').each (index, item)->
            id = $(item).data 'id'
            if id?
                $("#riders [data-id=#{ id }]").hide()

        $('#vote').on 'click', '.close', @removeRider
        $('#save').on 'click', @save

        if GP_START_DATE > Date.now()
            $('#countdown').show()
            @counter = new Countdown GP_START_DATE, @updateTimer

    updateTimer: (days, hours, mins, secs)=>
        fmt = (num)-> if num < 10 then '0' + num else num
        $('#countdown span').html if days > 0
                "#{days} дней #{fmt hours}:#{fmt mins}:#{fmt secs}"
            else
                "#{fmt hours}:#{fmt mins}:#{fmt secs}"

    addRider: (event, ui)=>
        target = $(event.target)

        if target.data('id')
            $("#riders [data-id=#{ target.data('id') }]").show()

        target
            .html(ui.draggable.find('.name').html() + '<a class="close">×</a>')
            .data('id', ui.draggable.data('id'))

        ui.draggable.hide()
        @save()

    removeRider: (event)=>
        item = $(event.target).parent()
        originalItem = $("#riders [data-id=#{ item.data('id') }]")
        item.removeData('id').removeAttr('data-id').html('')
        originalItem.show()
        @save()

    save: ->
        data = {}
        $('#vote .rider').each (index, item)->
            $item = $(item)
            id = $item.data 'id'
            data["#{index + 1}"] = id if id?

        $.ajax window.location.pathname + 'save/',
            type: 'POST'
            data:
                positions: JSON.stringify data
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
            success: ->
                Alertify.log.success('Vote saved')
            failure: ->
                Alertify.log.error('Vote save failed')

$ ->
    new App
