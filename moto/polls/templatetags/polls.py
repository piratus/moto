from django import template

register = template.Library()


@register.filter(is_safe=True)
def result_class(result):
    if not result.stage.finished:
        return ''

    return 'right' if result.was_right else 'wrong'
