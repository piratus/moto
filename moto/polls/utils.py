"""Utility functions for polls application"""
import json
from datetime import datetime
from django.http import HttpResponse
from django.utils.timezone import get_default_timezone


def json_success(data=None):
    """Send a json response"""
    data = data or {}
    data['success'] = True

    return HttpResponse(json.dumps(data), mimetype='application/json')


def json_error(message):
    """Send a json response, marked as error"""
    data = {
        'msg': message,
        'success': False
    }

    return HttpResponse(json.dumps(data), status=400,
            mimetype='application/json')


def time_left(until_date, until_time):
    """Calculate time left till something happens"""
    tz = get_default_timezone()
    until = datetime.combine(until_date, until_time)
    now = datetime.now(tz=tz).replace(tzinfo=None)

    if now > until:
        return (0, ) * 4

    delta = until - now
    hours, seconds = divmod(delta.seconds, 60 * 60)
    minutes, seconds = divmod(seconds, 60)

    return delta.days, hours, minutes, seconds
