from django.db import models
from django.contrib.auth.models import User


class Rider(models.Model):
    name = models.CharField(max_length=255)
    number = models.IntegerField()

    class Meta:
        verbose_name = 'rider'
        verbose_name_plural = 'riders'
        ordering = ('number', )

    def __unicode__(self):
        return self.name


class Stage(models.Model):
    name = models.CharField(max_length=255)
    short_name = models.CharField(max_length=5)
    date = models.DateField()
    time = models.TimeField()

    finished = models.BooleanField(default=False)

    class Meta:
        ordering = ('date', )

    def __unicode__(self):
        return self.name


class VoteResult(models.Model):
    user = models.ForeignKey(User)
    stage = models.ForeignKey(Stage)
    rider = models.ForeignKey(Rider)
    position = models.IntegerField()

    saved = models.DateTimeField(auto_now=True)

    was_right = models.BooleanField(default=False, editable=False)

    class Meta:
        ordering = ('stage', 'position', )
        unique_together = (('user', 'stage', 'position'),
                           ('user', 'stage', 'rider'), )

    def get_class(self):
        if not self.stage.finished:
            return ''

        return 'right' if self.was_right else 'wrong'


class StageResult(models.Model):
    stage = models.ForeignKey(Stage)
    rider = models.ForeignKey(Rider)
    position = models.IntegerField()

    class Meta:
        ordering = ('stage', 'position', )
        unique_together = (('stage', 'rider', ),
                           ('stage', 'position', ), )

        verbose_name = 'stage result'
        verbose_name_plural = 'stage results'

    def __unicode__(self):
        return '{stage} | {rider} ({position})'.format(
            stage=self.stage.short_name,
            rider=self.rider.name,
            position=self.position)

from django.dispatch import receiver


@receiver(models.signals.post_save, sender=StageResult)
def update_results(sender, **kwargs):
    """Update 'was_right' attribute in results"""
    result = kwargs['instance']
    result.stage.finished = True
    result.stage.save()
    VoteResult.objects.filter(stage=result.stage, rider=result.rider,
            position=result.position).update(was_right=True)


@receiver(models.signals.post_delete, sender=StageResult)
def revert_results(sender, **kwargs):
    result = kwargs['instance']
    VoteResult.objects.filter(stage=result.stage, rider=result.rider,
            position=result.position).update(was_right=False)
