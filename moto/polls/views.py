# coding: utf-8
import json
from django.shortcuts import render
from django.http import Http404
from django.contrib.auth.decorators import login_required
from django.db import transaction, IntegrityError

from .models import Rider, Stage, VoteResult
from .utils import time_left, json_success, json_error


def list_stages(request):
    """Display a list of all stages"""
    stages = Stage.objects.all()

    return render(request, 'polls/stages.html', {'stages': stages})


def view_poll(request, stage_name):
    """Display stage details"""
    riders = Rider.objects.all()
    stages = Stage.objects.all()
    try:
        current_stage = next(stage for stage in stages
                             if stage.short_name == stage_name)
    except StopIteration:
        raise Http404

    time = time_left(current_stage.date, current_stage.time)
    if current_stage.finished or (time[0] == 0 and time[1] == 0):
        return render(request, 'polls/stage_finished.html')

    if request.user.is_authenticated():
        votes = VoteResult.objects.select_related()\
            .filter(stage=current_stage, user=request.user).order_by('position')
    else:
        votes = []
    votes_map = {vote.position: vote.rider for vote in votes}
    table = [votes_map[position] if position in votes_map else None
             for position in xrange(1, 16)]

    return render(request, 'polls/stage_poll.html', {
        'riders': riders,
        'stages': stages,
        'current_stage': current_stage,
        'table': table,
    })


@login_required
@transaction.commit_manually
def save_poll(request, stage_name):
    """Save users poll"""
    positions = request.POST.get('positions')
    if not positions:
        return json_error(u'Пустой запрос')

    stage = Stage.objects.get(short_name=stage_name)

    time = time_left(stage.date, stage.time)
    if time[0] == 0 and time[1] == 0:
        return json_error(u'Голосование прекращается за час до начала гонки')

    positions = json.loads(positions)

    VoteResult.objects.filter(user=request.user, stage=stage).delete()
    try:
        for place, rider_id in positions.iteritems():
            VoteResult.objects.create(user=request.user, stage=stage,
                                      position=place, rider_id=rider_id)
    except IntegrityError:
        transaction.rollback()
        return json_error(u'Неправильный запрос')

    transaction.commit()

    return json_success()
