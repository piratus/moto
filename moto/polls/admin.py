from django.contrib import admin

from moto.polls.models import Rider, Stage, StageResult


class RiderAdmin(admin.ModelAdmin):
    list_display = ('number', 'name')
    list_display_links = ('name', )


class StageResultInline(admin.TabularInline):
    model = StageResult
    extra = 1


class StageAdmin(admin.ModelAdmin):
    list_display = ('short_name', 'name', 'date', 'time', 'finished')
    list_display_links = ('name', )
    list_editable = ('date', 'time')
    fields = (
        ('short_name', 'name', ),
        ('finished', 'date', 'time', ),
    )
    inlines = (StageResultInline, )

admin.site.register(Rider, RiderAdmin)
admin.site.register(Stage, StageAdmin)
