from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import RedirectView

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', RedirectView.as_view(url='/calendar/'), name='home'),
    url(r'^calendar/', include('moto.stats.urls')),
    url(r'^polls/', include('moto.polls.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('moto.accounts.urls')),
    url(r'^auth/', include('social_auth.urls')),
)


if settings.DEBUG:
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    urlpatterns += staticfiles_urlpatterns()
